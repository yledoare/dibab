#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL_ttf.h>
#include "SDL.h"
#include "bouton.xbm"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <dirent.h>

TTF_Font *police = NULL;
static void quit(int rc)
{
	TTF_CloseFont(police);
	TTF_Quit();
	SDL_Quit();
	exit(rc);
}

SDL_Surface *LoadXBM(SDL_Surface *screen, int w, int h, Uint8 *bits)
{
	SDL_Surface *bitmap;
	Uint8 *line;

	/* Allocate the bitmap */
	bitmap = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 1, 0, 0, 0, 0);
	if ( bitmap == NULL ) {
		fprintf(stderr, "Couldn't allocate bitmap: %s\n",
						SDL_GetError());
		return(NULL);
	}

	/* Copy the pixels */
	line = (Uint8 *)bitmap->pixels;
	w = (w+7)/8;
	while ( h-- ) {
		memcpy(line, bits, w);
		/* X11 Bitmap images have the bits reversed */
		{ int i, j; Uint8 *buf, byte;
			for ( buf=line, i=0; i<w; ++i, ++buf ) {
				byte = *buf;
				*buf = 0;
				for ( j=7; j>=0; --j ) {
					*buf |= (byte&0x01)<<j;
					byte >>= 1;
				}
			}
		}
		line += bitmap->pitch;
		bits += w;
	}
	return(bitmap);
}

int main(int argc, char *argv[])
{
	SDL_Surface *screen;
	SDL_Surface *bitmap;
	SDL_Surface *texte;
	Uint8  video_bpp;
	Uint32 videoflags;
	Uint8 *buffer;
	int i, k, done;
	SDL_Event event;
	Uint16 *buffer16;
        Uint16 color;
        Uint8  gradient;
	const SDL_VideoInfo *info;
	SDL_Color couleurNoire = {0, 0, 0};
	SDL_Rect position;
	SDL_Rect fullscreen;
	char adresse_ip[16];
	int fd;
	struct ifreq ifr;

	int n,font_size;
	struct dirent **namelist;
	char * skipfile;
	char font_name[255];

// asm volatile ("svc 1");

	// video_bpp = info->vfmt->BitsPerPixel;
	video_bpp = 8;
	videoflags = SDL_SWSURFACE;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
 	ifr.ifr_addr.sa_family = AF_INET;
 	strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
 	ioctl(fd, SIOCGIFADDR, &ifr);
 	close(fd);

	/* Initialize SDL */
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL: %s\n",SDL_GetError());
		return(1);
	}

	// SDL_SetVideoMode(240, 320, 8, SDL_FULLSCREEN);
	if(TTF_Init() == -1)
	{
     	  fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
    	  exit(EXIT_FAILURE);
	}
		
	/* Set Max video mode */
        info = SDL_GetVideoInfo();

	if ( (screen=SDL_SetVideoMode(info->current_w,info->current_h,video_bpp,videoflags)) == NULL ) {
		fprintf(stderr, "Couldn't set %dx%dx%d video mode: %s\n",info->current_w,info->current_h, video_bpp, SDL_GetError());
		quit(2);
	}
		fprintf(stderr, "Set %dx%dx%d video mode\n",info->current_w,info->current_h, video_bpp);
	fullscreen.w=info->current_w;
	fullscreen.h=info->current_h;
	fullscreen.x=0;
	fullscreen.y=0;
	SDL_FillRect(screen, &fullscreen,SDL_MapRGB(screen->format,255,0,0));
	SDL_UpdateRect(screen, 0, 0, 0, 0);
	/* Load the bitmap */
	bitmap = LoadXBM(screen, bouton_width, bouton_height,
					(Uint8 *)bouton_bits);
	if ( bitmap == NULL ) {
		quit(1);
	}

	
   n = scandir("/media/mmc", &namelist, 0, alphasort);
   if (n < 0)
	{
      perror("scandir : cannot open /media/mmc");
      quit(2);
	}
   else
     for(i=0;i<n;i++) 
   {
    // printf("%s\n", namelist[i]->d_name);
    skipfile=strrchr(namelist[i]->d_name,'.');
    if(skipfile == NULL) continue;
    if(strncmp(skipfile,".ttf",4)) continue;
    if(sscanf(namelist[i]->d_name,"concours-%d.ttf",&font_size) == EOF) exit(1);
    sprintf(font_name,"/media/mmc/%s",namelist[i]->d_name);
    printf("Font is : %s , size is %d \n",font_name,font_size);
    break;
  }
	police = TTF_OpenFont(font_name, font_size);
	
	strncpy(adresse_ip, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr) ,16);
	texte = TTF_RenderText_Blended(police, adresse_ip, couleurNoire);

        position.x = (info->current_w/2-texte->w/2);
        position.y = (info->current_h/2-texte->h/2);
        SDL_BlitSurface(texte, NULL, screen, &position); 
	/* Wait for a keystroke */
	done = 0;
					SDL_Rect dst;
					dst.x = (info->current_w)-(bitmap->w)-6;
					dst.y = (info->current_h)-(bitmap->h)-6;
					dst.w = bitmap->w;
					dst.h = bitmap->h;
					printf("X=%d, Y=%d, W=%d, H=%d \n",dst.x, dst.y,dst.w,dst.h);
					SDL_BlitSurface(bitmap, NULL,
								screen, &dst);
					SDL_UpdateRects(screen,1,&dst);
        SDL_Flip(screen);
	while ( !done ) {
		/* Check for events */
		while ( SDL_PollEvent(&event) ) {
			switch (event.type) {
				case SDL_MOUSEBUTTONDOWN: {
					// if((event.button.x >= dst.x)&&(event.button.x<(dst.x+dst.w)))
					if((event.button.x >= dst.x)&&(event.button.x<(dst.x+dst.w))&&(event.button.y>=dst.y)&&(event.button.y<(dst.y+dst.h)))
					{
					// done = 1;
			texte = TTF_RenderText_Blended(police, "Yann Le Doare", couleurNoire);
        position.x = (info->current_w/2-texte->w/2);
        position.y = (info->current_h/4-texte->h/4);
        SDL_BlitSurface(texte, NULL, screen, &position); 
       SDL_Flip(screen);
					}
					printf("X=%d, Y=%d \n",event.button.x, event.button.y);
					}
					break;
				case SDL_KEYDOWN:
					/* Any key press quits the app... */
					done = 1;
					break;
				case SDL_QUIT:
					done = 1;
					break;
				default:
					break;
			}
		}
	}
	quit(0);
}
